#include <iostream> 
#include <stdio.h>
#include <iostream>


                                                                                ////////////////////////////////////
#define LEFT_DOWN   table[line + 1][column - 1]                                 //[-1 L_U -1] [-1 U 0] [-1 R_U +1]//
#define DOWN        table[line + 1][column]                                     //[0   L  -1] [0^__^0] [0   R  +1]//
#define RIGHT_DOWN  table[line + 1][column + 1]                                 //[+1 L_D -1] [+1 D 0] [+1 R_D +1]//
#define RIGHT       table[line][column + 1]                                     ////////////////////////////////////
#define RIGHT_UP    table[line - 1][column + 1]
#define UP          table[line - 1][column]
#define LEFT_UP     table[line - 1][column - 1]
#define LEFT        table[line][column - 1]
 
const int LINES = 24, COLUMNS = 80;
const char amoeba = '*';
 
class World
{
    private:
        bool table[LINES][COLUMNS];
        bool auxiliaryTable[LINES][COLUMNS];                                    //why? hz...
    public:
        void randomizeTable();
        void step();
        void showTable();
};
 
void World::randomizeTable()
{
    srand(time(NULL));
    for(int line = 0; line < LINES; ++line)
        for(int column = 0; column < COLUMNS; ++column)
            /*auxiliaryTable[line][column] = (*/table[line][column] = rand() % 2/*)*/;
}
 
void World::step()                                                              //Caution!
{                                                                               //Probably bad code ='(
    for(int line = 0; line < LINES; ++line)
        for(int column = 0; column < COLUMNS; ++column)
        {
            int neighbors = 0;
            if((line == 0) && (column == 0))                                    //left up
                neighbors += DOWN + RIGHT_DOWN + RIGHT;
            else if((line == 0) && (column == (COLUMNS - 1)))                   //right up
                neighbors += LEFT + LEFT_DOWN + DOWN;
            else if((line == (LINES - 1)) && (column == (COLUMNS - 1)))         //right down
                neighbors += UP + LEFT_UP + LEFT;
            else if((line == (LINES - 1)) && (column == 0))                     //left down
                neighbors += RIGHT + RIGHT_UP + UP;
            else if(column == 0)                                                //left
                neighbors += DOWN + RIGHT_DOWN + RIGHT + RIGHT_UP + UP;
            else if(line == 0)                                                  //up
                neighbors += LEFT + LEFT_DOWN + DOWN + RIGHT_DOWN + RIGHT;
            else if(column == (COLUMNS - 1))                                    //right
                neighbors += UP + LEFT_UP + LEFT + LEFT_DOWN + DOWN;
            else if(line == (LINES - 1))                                        //down
                neighbors += RIGHT + RIGHT_UP + UP + LEFT_UP + LEFT;
            else
                neighbors += LEFT_DOWN + DOWN + RIGHT_DOWN + RIGHT + RIGHT_UP + UP + LEFT_UP + LEFT;
            if(table[line][column])
                ((neighbors > 3) || (neighbors < 2)) ? auxiliaryTable[line][column] = 0 : auxiliaryTable[line][column] = 1;
            else
                (neighbors == 3) ? auxiliaryTable[line][column] = 1 : auxiliaryTable[line][column] = 0;
        }
    for(int line = 0; line < LINES; ++line)
        for(int column = 0; column < COLUMNS; ++column)
            table[line][column] = auxiliaryTable[line][column];
}
 
void World::showTable()
{
    for(int line = 0; line < LINES; ++line)
        for(int column = 0; column < COLUMNS; ++column)
            (table[line][column]) ? std::cout << amoeba : std::cout << ' ';
            //printf((table[line][column]) ? ("%X", amoeba) : (" "));
            //(table[line][column]) ? printf("•") : printf(" ");
}

 
int main()
{
    /*char c_game; //y/n
    std::cout << "Start game? (y/n):\n";
    std::cin >> c_game;
    if(c_game != 'y')
        return 0;*/
    World w1;
    w1.randomizeTable();
    bool a=true;
    while(true)
    {
        w1.step();
        system("cls");
        w1.showTable();
        //Sleep(300);
    }
    return 0;
}